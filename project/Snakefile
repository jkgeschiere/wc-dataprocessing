# -*- python -*-

ID_AMOUNT = ["1", "2"]

rule all:
    input:
        "output/out.html"
    message:
        "Getting all outputfiles"


rule add_ncbi_to_string:
    input:
         script_file = "scripts/NCBIToString.bsh",
         IDs = "input/idNumbers.txt"
    output:
         "output/ncbiGeneIdStrings.txt"
    message:
         "Splitting IDs from {input.IDs} and Adding 'ncbi-geneid:' to all IDs"
    shell:
         "java bsh.Interpreter {input.script_file} {input.IDs}"


rule bconv_rest:
    input:
        "output/ncbiGeneIdStrings.txt"
    output:
        "output/bconvREST.txt"
    message:
        "Changing NCBI Protein IDs from {input} to KEGG Protein IDs"
    run:
        ncbi_string = ""
        target_value = "hsa"
        for line in open(input[0], "r"):
            line = line.split("\n")
            for item in line:
                ncbi_string += item + "+"

        shell("curl -H 'Accept: application/xml' -H 'Content-Type: application/xml' -X "
              "GET http://rest.kegg.jp/conv/{target_value}/{ncbi_string} > {output}")


rule extract_gene_name:
    input:
         script_file = "scripts/ExtractName.bsh",
         gene_list = "output/bconvREST.txt"
    output:
         "output/geneNamesList.txt"
    message:
         "Extracting gene names from {input.gene_list} using the regular expression: 'hsa:[0-9]{{4,}}' and group value: '0'"
    run:
        regex = "'hsa:[0-9]{4,}'"
        group_value = "0"
        outputName = "output/geneNamesList.txt"

        shell("java bsh.Interpreter {input.script_file} {input.gene_list} {regex} {group_value} {outputName}")


rule get_pathway_by_gene:
    input:
        "output/geneNamesList.txt"
    output:
        expand("output/pathwayID{number}.txt", number=ID_AMOUNT)
    message:
        "Getting pathway IDs from {input} using the gene names"
    run:
        value = 1
        for line in open(input[0], "r"):
            line = line[1:-1]
            line = line.split(", ")
            for item in line:
                print(item)
                shell("curl -H 'Accept: application/xml' -H 'Content-Type: application/xml'"
              " -X GET http://rest.kegg.jp/link/pathway/{item} > output/pathwayID{value}.txt")
                value += 1


rule extract_pathway_name:
    input:
         script_file = "scripts/ExtractName.bsh",
         pathway_id = expand("output/pathwayID{number}.txt", number=ID_AMOUNT)
    output:
         pathway_extraction = expand("output/pathwayExtraction{number}.txt", number=ID_AMOUNT)
    message:
         "Extracting first pathway from {input.pathway_id} using regular expression and group values"
    run:
        regex = "'path:[0-9a-z]{5,}'"
        group_value = "0"
        value = 1
        for pathway_id in input.pathway_id:
            outputName = "output/pathwayExtraction" + str(value) + ".txt"
            shell("java bsh.Interpreter {input.script_file} {pathway_id} {regex} {group_value} {outputName}")
            value += 1


rule get_pathway_entry_and_image:
    input:
        files = expand("output/pathwayExtraction{number}.txt", number=ID_AMOUNT)
    output:
        pathway_entry = expand("output/pathDescRest{number}.txt", number=ID_AMOUNT),
        pathway_image = expand("output/pathwayImage{number}.jpg", number=ID_AMOUNT)
    message:
        "Creating pathway_entries and pathway images from {input.files}"
    run:
        value = 1
        for file in input.files:
            for line in open(file, "r"):
                line = line[1:-1]
                line = line.split(", ")
                pathway_id = line[0]
                outputEntry = "output/pathDescRest" + str(value) + ".txt"
                outputImage = "output/pathwayImage" + str(value) + ".jpg"

                shell("curl -H 'Accept: application/xml' -H 'Content-Type: application/xml'"
                    " -X GET http://rest.kegg.jp/get/{pathway_id} > {outputEntry}")
                shell("curl -H 'Accept: application/xml' -H 'Content-Type: application/xml'"
                    " -X GET http://rest.kegg.jp/get/{pathway_id}/image > {outputImage}")
                value += 1


rule merge_desc:
    input:
         script_file = "scripts/MergeDESC.bsh",
         pathway_desc = expand("output/pathDescRest{number}.txt", number=ID_AMOUNT)
    output:
          "output/pathDescRest.txt"
    message:
           "Merging pathway entry's from {input.pathway_desc} descending"
    shell:
         "java bsh.Interpreter {input.script_file} {input.pathway_desc}"


rule report:
    input:
        path_id = expand("output/pathwayExtraction{number}.txt", number=ID_AMOUNT),
        path_desc = "output/pathDescRest.txt",
        path_image = expand("output/pathwayImage{number}.jpg", number=ID_AMOUNT)
    output:
        "output/out.html"
    message:
        "Reporting {input} to a HTML page"
    run:
        from snakemake.utils import report
        n_calls = 0
        with open(input[0]) as f:
            for line in f:
                if not line.startswith("#"):
                    n_calls += len(line.split(","))

        report("""
        Pathway images and descriptions
        ===================================

        Pathway ID's were taken from the KEGG database using their corresponding NCBI-gene ID's
        and pathway images and pathway descriptions were saved using kegg.rest.api

        This resulted in {n_calls} found pathways (see path_id_), where an image and description of the first 
        pathway of each of the given NCBI ID's is shown.
        """, output[0], metadata="Author: J.K. Geschiere", **input)
