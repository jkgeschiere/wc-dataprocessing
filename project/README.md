# Project

This project exists of a snakemake workflow which uses NCBI-gene ID's
to get their corresponding pathway ID's from the KEGG database using the kegg rest api.

## Installation
This project uses BeanShell scripts to work, therefore the bsh.jar must be installed beforehand.

To install as an extension place the bsh.jar file in your 
$JAVA_HOME/jre/lib/ext folder.  (OSX users: place the bsh.jar in 
/Library/Java/Extensions or ~/Library/Java/Extensions for individual users.)

Or add BeanShell to your classpath like this:

unix:
```bash
export CLASSPATH=$CLASSPATH:bsh-2.0b2.jar
```

windows:
```bash
set classpath %classpath%;bsh-2.0b2.jar
```

## Usage

To use this workflow put the NCBI-gene ID's inside the "input/idNumbers.txt"
separate the values by ',' or set them on a different line.
An example of the values can already be found inside "input/idNumbers.txt".

Afterwards go to this map inside your terminal:

```bash
cd "/path/to/project_directory"
```

Create an vertual environment if you do not have one:
```bash
virtualenv -p /usr/bin/python3 venv
source venv/bin/activate

#install snakemake
pip3 install snakemake
```

or activate the existing environment:
```bash
source venv/bin/activate
```

And use the following command:
```bash
snakemake all --cores 4
```

Deactivate the virtualenv when your done:
```bash
deactivate
```

## Support
For any questions contact [J.K. Geschiere](j.k.geschiere@st.hanze.nl)
